module signext_tb();
	logic [31:0] a;
	logic [63:0] y;

	signext dut(a, y);
	
	
	initial
		begin
			a = 32'hf85fffff; #20ns;
			a = 32'hf81fffff; #20ns;
			a = 32'hb40fffff; #20ns;
			a = 32'hb47fffff; #20ns;
		end
endmodule

