module flopr #(parameter N = 64) (
	input logic [N-1:0] d,
	input logic clk,
	input logic reset,
	output logic [N-1:0] q
);

always_ff @(posedge clk, posedge reset)
		if(reset == 1'b1) q <= '0;
		else q <= d;
endmodule
