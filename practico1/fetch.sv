module fetch(
		input logic PCSrc_F ,clk,reset,
		input logic [63:0] PCBranch_F,
		output logic [63:0] imem_addr_F);
		
		// Constants
		localparam N = 64;
		localparam NUM = 4;
		localparam SUM = 4'b0010;
		
		//Auxiliar variables
		logic zero;
		logic [N-1 : 0] res,out; 
		
		mux2 #(N) m(PCBranch_F,res,PCSrc_F,out);
		flopr #(N) f(out,clk,reset,imem_addr_F);
		add #(N) a(imem_addr_F, NUM,res);

endmodule