module execute(input logic AluSrc,
					input logic [3:0] AluControl,
					input logic [63:0] PC_E,signImm_E,readData1_E,readData2_E,
					output logic [63:0] PCBranch_E,aluResult_E,writeData_E,
					output logic zero_E);

		localparam N = 64;
		
		logic [N-1:0] shift,mux_res;
		
		shift2 #(N) (signImm_E,shift);
		add #(N) (shift,PC_E,PCBranch_E);
		
		mux2 #(N) (signImm_E,readData2_E,AluSrc,mux_res);

		alu #(N) (readData1_E,mux_res,AluControl,zero_E,aluResult_E);
		
		assign writeData_E = readData2_E;

endmodule
