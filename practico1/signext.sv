module signext (
	input logic [31:0] a,
	output logic [63:0] y
);

logic [10:0] opcode;
assign opcode = a[31:21];

always_comb
casez(opcode)
	11'b111110000?0: y = {{55{a[20]}}, a[20:12]};
	11'b10110100???: y = {{{43{a[23]}}, a[23:5]}, 2'b00};
	default: y = 0;
endcase 
endmodule 