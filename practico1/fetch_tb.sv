module fetch_tb();
	
	logic clk = 0, reset = 1;
	logic [63:0] PCBranch_F = 100;
	logic PCSrc_F = 1;
	
	fetch f(PCSrc_F,clk,reset,PCBranch_F);
	
	always begin
		clk = ~clk; #10;
		reset = ~reset;#5;
		PCSrc_F = ~PCSrc_F;#100;
	end 
	
	

endmodule