module flopr_tb();
	logic clk = 0;
	logic reset;

	localparam N = 4;
	logic [N-1:0] d, q;

flopr #(4) dut(d, clk, reset, q);
	
	always
		begin
			clk = ~clk; #5ns;
		end
 
	initial begin
		reset = 1;
		#5ns;
		// d = 0; #10ns;
		// d = 1; #10ns;
		// d = 2; #10ns;
		// d = 3; #10ns;
		// d = 4; #10ns;
		// d = 5; #10ns;
		// d = 6; #10ns;
		// d = 7; #10ns;
		// d = 8; #10ns;
		// d = 9; #10ns;
		// d = 10; #10ns;
		for(int i=0; i<10;i++) begin
			d = i;
			#10ns;
		end
		$stop;
	end
	initial 
			#50ns reset = 0;
endmodule
