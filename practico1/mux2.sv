module mux2 #(parameter N = 64) (
	input logic [N-1:0] a,b,
	input logic select,
	output logic [N-1:0] q);

	assign q = (select) ? a : b;
endmodule