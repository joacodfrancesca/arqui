module shift2 #(parameter N = 64) (
	input logic [N-1:0] a,
	output logic [N-1:0] q);

	assign q = a*2;
endmodule