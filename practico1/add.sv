module add #(parameter N = 64) (
	input logic [N-1:0] a,b,
	output logic [N-1:0] q);

	assign q = a + b;
endmodule