module maindec_tb();
	logic [10:0] op;
	logic Reg2Loc;
	logic MemtoReg;
	logic ALUSrc;
	logic RegWrite;
	logic MemRead;
	logic Branch;
	logic [1:0] ALUOp;

	maindec dut(op, Reg2Loc, MemtoReg, ALUSrc, RegWrite, MemRead, MemWrite, Branch, ALUOp);
	
	initial 
		begin
			op = 11'b101_1010_0???; #10ns;
			op = 11'b111_1100_0010; #10ns;
			op = 11'b100_0101_0000; #10ns;
		end
endmodule
